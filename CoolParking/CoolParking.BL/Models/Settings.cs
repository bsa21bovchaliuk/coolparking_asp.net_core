﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal InitialBalance { get; } = 0.0M;

        public static int ParkingCapacity { get; } = 10;

        public static int PaymentPeriod { get; } = 5;

        public static int LoggingPeriod { get; } = 60;

        public static ReadOnlyDictionary<VehicleType, decimal> Tariffs { get; } = new ReadOnlyDictionary<VehicleType, decimal>(
            new Dictionary<VehicleType, decimal>()
            {
                { VehicleType.PassengerCar, 2.0M},
                { VehicleType.Truck, 5.0M},
                { VehicleType.Bus, 3.5M},
                { VehicleType.Motorcycle, 1.0M}
            });

        public static decimal FineRatio { get; } = 2.5M;
    }
}