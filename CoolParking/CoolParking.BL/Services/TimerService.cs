﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using CoolParking.BL.Interfaces;
using System;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService, IDisposable
    {
        public double Interval { get => systemTimer.Interval; set => systemTimer.Interval = value; }

        public event ElapsedEventHandler Elapsed
        {
            add { systemTimer.Elapsed += value; }
            remove { systemTimer.Elapsed -= value; }
        }

        private readonly Timer systemTimer;

        public TimerService()
        {
            systemTimer = new Timer();
        }

        public TimerService(double interval) : this()
        {
            Interval = interval;
        }

        public void Dispose()
        {
            systemTimer.Dispose();
        }

        public void Start()
        {
            systemTimer.Start();
        }

        public void Stop()
        {
            systemTimer.Stop();
        }
    }
}