﻿using System;
using System.Threading.Tasks;

namespace CoolParking.Client
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var parkingFacade = new ParkingFacade();
            await parkingFacade.Start();
        }
    }
}
