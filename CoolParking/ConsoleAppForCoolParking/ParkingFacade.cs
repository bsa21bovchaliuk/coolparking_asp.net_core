﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System;
using System.IO;
using System.Reflection;

namespace ConsoleAppForCoolParking
{
    class ParkingFacade
    {
        private readonly string logFilePath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";

        private readonly ParkingService parkingService;
        private readonly TimerService withdrwTimer;
        private readonly TimerService logTimer;
        private readonly LogService logService;

        public ParkingFacade()
        {
            File.WriteAllText(logFilePath, string.Empty);

            withdrwTimer = new TimerService(TimeSpan.FromSeconds(Settings.PaymentPeriod).TotalMilliseconds);
            logTimer = new TimerService(TimeSpan.FromSeconds(Settings.LoggingPeriod).TotalMilliseconds);
            logService = new LogService(logFilePath);

            parkingService = new ParkingService(withdrwTimer, logTimer, logService);
            withdrwTimer.Start();
            logTimer.Start();
        }

        public void PrintParkingBalance()
        {
            Console.WriteLine("Parking balance = {0}", parkingService.GetBalance().ToString());
        }

        public void PrintCurrentPeriodIncome()
        {
            decimal income = 0.0M;
            foreach (var transaction in parkingService.GetLastParkingTransactions())
            {
                income += transaction.Sum;
            }

            Console.WriteLine("Current period income = {0}", income);
        }

        public void PrintParkingFreePlaces()
        {
            Console.WriteLine("Free {0} of {1} places",
                parkingService.GetFreePlaces(),
                parkingService.GetCapacity());
        }

        public void PrintCurrentPeriodTransactions()
        {
            Console.WriteLine("Current period transactions:");
            foreach (var transaction in parkingService.GetLastParkingTransactions())
            {
                Console.WriteLine(transaction.ToString());
            }
        }

        public void PrintTransactionHistory()
        {
            Console.WriteLine("Transaction History:");
            Console.WriteLine(parkingService.ReadFromLog());
        }

        public void PrintVehiclesList()
        {
            Console.WriteLine("Vehicles on parking:");
            foreach (var vehicle in parkingService.GetVehicles())
            {
                Console.WriteLine(vehicle.ToString());
            }
        }

        public void AddVehicle()
        {
            Console.WriteLine("Vyhicle type:");
            Console.WriteLine("1 - {0}", VehicleType.PassengerCar.ToString());
            Console.WriteLine("2 - {0}", VehicleType.Truck.ToString());
            Console.WriteLine("3 - {0}", VehicleType.Bus.ToString());
            Console.WriteLine("4 - {0}", VehicleType.Motorcycle.ToString());
            var vehicleTypeString = Console.ReadLine();
            Console.WriteLine("Enter vehicle balance:");
            var balanceString = Console.ReadLine();
            try
            {
                int vehicleType = int.Parse(vehicleTypeString) - 1;
                decimal balance = decimal.Parse(balanceString);

                parkingService.AddVehicle(new Vehicle((VehicleType)vehicleType, balance));
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e.Message);
            }

        }

        public void RemoveVehicle()
        {
            Console.WriteLine("Enter id of vehicle to remove:");
            var id = Console.ReadLine();
            try
            {
                parkingService.RemoveVehicle(id);
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void TopUpVehicleBalance()
        {
            Console.WriteLine("Enter id vehicle to TopUp:");
            var id = Console.ReadLine();
            Console.WriteLine("Enter money to TopUp:");
            var sumString = Console.ReadLine();
            try
            {
                decimal sum = decimal.Parse(sumString);
                parkingService.TopUpVehicle(id, sum);
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
